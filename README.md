# @justojs/assert

[![NPM version](https://img.shields.io/npm/v/@justojs/assert.svg)](https://npmjs.org/package/@justojs/assert)
[![Total downloads](https://img.shields.io/npm/dt/@justojs/assert.svg)](https://npmjs.org/package/@justojs/assert)

An assertion library.

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Engineered in Valencia, Spain, EU by Justo Labs.*

## Use

The package must be imported as follows:

```
import assert from "@justojs/assert";
```

## Type assertions

To check whether a value is of a type or another, we can use:

```
//string
assert(value).isText()
assert(value).isNotText()

assert(value).isString()
assert(value).isNotString()

//boolean
assert(value).isBool()
assert(value).isNotBool()

assert(value).isBoolean()
assert(value).isNotBoolean()

//number
assert(value).isNum()
assert(value).isNotNum()

assert(value).isNumber()
assert(value).isNotNumber()

//null
assert(value).isNil()
assert(value).isNotNil()

assert(value).isNull()
assert(value).isNotNull()

//object
assert(value).isMap()
assert(value).isNotMap()

assert(value).isObject()
assert(value).isNotObject()

//array
assert(value).isList()
assert(value).isNotList()

assert(value).isArray()
assert(value).isNotArray()

//set
assert(value).isSet()
assert(value).isNotSet()

//function
assert(value).isFn()
assert(value).isNotFn()

assert(value).isFunction()
assert(value).isNotFunction()

//callable object
assert(value).isCallable()
assert(value).isNotCallable()

//promise
assert(value).isPromise()
assert(value).isNotPromise()
```

For checking if a value is instance of a given class, we can use:

```
assert(value).isInstanceOf(className)
assert(value).isInstanceOf(class)

assert(value).isNotInstanceOf(className)
assert(value).isNotInstanceOf(class)
```

Instead of `isInstanceOf()` and `isNotInstanceOf()`, we also can use their aliases `is()` and `isNot()`.

## eq(), ne(), lt(), le(), gt() and ge()

With `eq()`, `ne()`, `lt()`, `le()`, `gt()` and `gt()` we can check whether a value is equal to, not equal to, less than, less than or equal to, greater than and greater than or equal to another.

```
assert(value1).eq(value2)
assert(value1).ne(value2)
assert(value1).lt(value2)
assert(value1).le(value2)
assert(value1).gt(value2)
assert(value1).ge(value2)
```

## sameAs() and notSameAs()

With `sameAs()` and `notSameAs()` we can check whether a value is same as other:

```
assert(value).sameAs(value)
assert(value).notSameAs(value)
```

## similarTo() and notSimilarTo()
With `similarTo()` and `notSimilarTo()` we check whether an array is equal to another, not keeping in mind the order of the items:

```
assert(array1).similarTo(array2)
assert(array1).notSimilarTo(array2)
```

Examples:

```
assert([1, 2, 3]).similarTo([2, 1, 3])  //true
assert([1, 2, 3]).similarTo([3, 2, 1])  //true
assert([1, 2, 3]).similarTo([1, 3, 2])  //true
assert([1, 2, 3]).similarTo([1, 2, 4])  //false
```

## between() and notBetween()

With `between()` and `notBetween()`, we check whether a value is into a range:

```
assert(value).between(start, end)
assert(value).notBetween(start, end)
```

## includes(), notIncludes() and doesNotInclude()

With `includes()`, `notIncludes()` and `doesNotInclude()` we check whether a string contains a given text or an array contains an item:

```
assert(value).includes(item)
assert(value).notIncludes(item)
assert(value).doesNotInclude(item)
```

## has(), notHas() and doesNotHave()

With `has()`, `notHas()` and `doesNotHave()` we can check whether an object contains one or several fields:

```
assert(value).has(field)
assert(value).has(fields)
assert(value).notHas(field)
assert(value).notHas(fields)
assert(value).doesNotHave(field)
assert(value).doesNotHave(fields)
```

Examples:

```
assert({one: 1, two: 2, three: 3}).has("one")
assert({one: 1, two: 2, three: 3}).has(["one", "two"])
assert({one: 1, two: 2, three: 3}).has({
  one: 1,
  two: 2
})
```

## forEach()

When the value under assertion is a list, we can use `forEach()` for checking the items:

```
assert(value).forEach(fields)
assert(value).forEach(func)
```

When an object is passed as argument, the fields must have the given values.
So, for example, the following one:

```
assert(dev.getUserWorks(work.user)).isList().isNotEmpty().forEach({user: work.user});
```

is similar to:

```
const ww = dev.getUserWorks(work.user);
assert(ww).isList().isNotEmpty();
for (let w of ww) assert(w).has({user: work.user});
```

When a function is passed, each item is passed to the function.
Example:

```
assert(resp.json()).isList().isNotEmpty().forEach(function(o) {
  assert(o.req).isObject().mem("req").eq(req);
});
```

## isEmpty() and isNotEmpty()

With `isEmpty()` and `isNotEmpty()` we can check whether an object is empty as for example a list or string:

```
assert(value).isEmpty()
assert(value).isNotEmpty()
```

## len() and notLen()

With `len()` and `notLen()` we can check the string, array or object length:

```
assert(value).len(size)
assert(value).notLen(size)
```

Also possible getting the length from other list:

```
assert([1, 2, 3]).len([1, 3, 5])  //ok, both with length equal to 3
assert([1, 2, 3]).len([1, 5])     //nop, one with length 3; and other 2
```

## like() and notLike()

With `like()` and `notLike()` we check whether a value matches a pattern:

```
assert(value).like(pattern)
assert(value).notLike(pattern)
```

## startsWith(), notStartsWith() and doesNotStartWith()

With `startsWith()`, we check whether a value starts with a given prefix:

```
assert(value).startsWith(prefix)
assert(value).notStartsWith(prefix)
assert(value).doesNotStartWith(prefix)
```

## endsWith(), notEndsWith() and doesNotEndWith()

With `endsWith()`, we check whether a value ends with a given suffix:

```
assert(value).endsWith(suffix)
assert(value).notEndsWith(suffix)
assert(value).doesNotEndWith(suffix)
```

## Function assertions

To check whether a function raises an error, we can use `raises()`, `notRaises()` and `doesNotRaise()`:

```
assert(fn).raises()
assert(fn).raises(err)
assert(fn).notRaises()
assert(fn).notRaises(err)
assert(fn).doesNotRaise()
assert(fn).doesNotRaise(err)
```

The function to check must not expect arguments.

Example:

```
assert(function() {
  //code
}).raises()

assert(function() {
  //code
}).raises("My custom error.")
```

### mem() and item()

Sometimes, we need to assert multiple members of a source value.
With the `mem()` method, we can get the value for one of its members:

```
mem(name:string) : ValueWrapper
mem(idx:number) : ValueWrapper
mem(...:string|number) : ValueWrapper
```

Examples:

```
value = {x: 123, y: 456};
assert(value).mem("x").eq(123);                                           //ok
assert(value).isMap().mem("x").isNum().eq(123).mem("y").isNum().eq(456);  //ok

value = ["zero", "one", "two", "three"];
assert(value).mem(1).eq("one").mem(0).eq("zero");                         //ok

value = {x: ["zero", "one", "two", "three"]};
assert(value).mem("x", 1).eq("one");                                      //ok
```

The `mem()` method always returns with respect to the source value passed to `assert()`.
And the assertions are respect to the last member got from `mem()`.

We can use the alias `item()` too.
Generally it is used when accessing to a `list` or `array`.

## Chaining assertions

We can chain assertion methods for the same value such as the following example:

```
assert(number).ge(10).le(25)
//similar to
assert(number).ge(10)
assert(number).le(25)
```
