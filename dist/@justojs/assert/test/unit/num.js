"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

var _chai = require("chai");

const ja = _dogmalangmin.dogma.use(require("../../../../@justojs/assert"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    let val;(0, _justo.init)(() => {
      {
        val = ja(123);
      }
    });(0, _justo.test)("isNum() - error", params => {
      /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("params", params, null);{
        _chai.assert.strictEqual((0, _dogmalangmin.bind)(val, params.meth)(), val);
      }
    }).forEach({ ["sub"]: "isNum()", ["meth"]: "isNum" }, { ["sub"]: "isNumber()", ["meth"]: "isNumber" });(0, _justo.test)("isNotNum()", params => {
      /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("params", params, null);{
        _chai.assert.throws(() => {
          {
            (0, _dogmalangmin.bind)(val, params.meth)();
          }
        });
      }
    }).forEach({ ["sub"]: "isNotNum()", ["meth"]: "isNotNum" }, { ["sub"]: "isNotNumber()", ["meth"]: "isNotNumber" });
  }
});