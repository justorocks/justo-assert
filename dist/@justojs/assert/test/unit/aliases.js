"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

var _chai = require("chai");

const ja = _dogmalangmin.dogma.use(require("../../../../@justojs/assert"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.test)("isString()", () => {
      {
        const val = ja("text");_chai.assert.strictEqual(val.isString(), val);
      }
    });(0, _justo.test)("isNotString()", () => {
      {
        const val = ja(true);_chai.assert.strictEqual(val.isNotString(), val);
      }
    });(0, _justo.test)("isNull()", () => {
      {
        const val = ja(null);_chai.assert.strictEqual(val.isNull(), val);
      }
    });(0, _justo.test)("isNotNull()", () => {
      {
        const val = ja("text");_chai.assert.strictEqual(val.isNotNull(), val);
      }
    });(0, _justo.test)("isArray()", () => {
      {
        const val = ja([1, 2, 3]);_chai.assert.strictEqual(val.isArray(), val);
      }
    });(0, _justo.test)("isNotArray()", () => {
      {
        const val = ja({});_chai.assert.strictEqual(val.isNotArray(), val);
      }
    });(0, _justo.test)("isObject()", () => {
      {
        const val = ja({});_chai.assert.strictEqual(val.isObject(), val);
      }
    });(0, _justo.test)("isNotObject()", () => {
      {
        const val = ja("text");_chai.assert.strictEqual(val.isNotObject(), val);
      }
    });(0, _justo.test)("doesNotInclude()", () => {
      {
        const val = ja([1, 2, 3]);_chai.assert.strictEqual(val.doesNotInclude(5), val);
      }
    });(0, _justo.test)("doesNotStartWith()", () => {
      {
        const val = ja("supertramp");_chai.assert.strictEqual(val.doesNotStartWith("tramp"), val);
      }
    });(0, _justo.test)("doesNotEndWith()", () => {
      {
        const val = ja("supertramp");_chai.assert.strictEqual(val.doesNotEndWith("super"), val);
      }
    });
  }
});