"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

var _chai = require("chai");

const ja = _dogmalangmin.dogma.use(require("../../../../@justojs/assert"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.test)("isFn()", params => {
      /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("params", params, null);{
        const val = ja(_dogmalangmin.dogma.nop());_chai.assert.strictEqual((0, _dogmalangmin.bind)(val, params.meth)(), val);
      }
    }).forEach({ ["sub"]: "isFn()", ["meth"]: "isFn" }, { ["sub"]: "isFunction()", ["meth"]: "isFunction" });(0, _justo.test)("isNotFn()", params => {
      /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("params", params, null);{
        const val = ja(_dogmalangmin.dogma.nop());_chai.assert.throws(() => {
          {
            (0, _dogmalangmin.bind)(val, params.meth)();
          }
        });
      }
    }).forEach({ ["sub"]: "isNotFn()", ["meth"]: "isNotFn" }, { ["sub"]: "isNotFunction()", ["meth"]: "isNotFunction" });(0, _justo.test)("isCallable()", () => {
      {
        const val = ja(_dogmalangmin.dogma.nop());_chai.assert.strictEqual(val.isCallable(), val);
      }
    });(0, _justo.test)("isNotCallable()", () => {
      {
        const val = ja(_dogmalangmin.dogma.nop());_chai.assert.throws(() => {
          {
            val.isNotCallable();
          }
        });
      }
    });(0, _justo.suite)("raises()", () => {
      {
        (0, _justo.test)("raises()", () => {
          {
            const val = ja(() => {
              {
                _dogmalangmin.dogma.raise("an error.");
              }
            });_chai.assert.strictEqual(val.raises(), val);
          }
        });(0, _justo.test)("raises() - error when must raise error", () => {
          {
            const val = ja(_dogmalangmin.dogma.nop());_chai.assert.throws(() => {
              {
                val.raises();
              }
            });
          }
        });(0, _justo.test)("raises() - error when error raised but must be other error", () => {
          {
            const val = ja(() => {
              {
                _dogmalangmin.dogma.raise("my error msg.");
              }
            });_chai.assert.throws(() => {
              {
                val.raises("other error msg.");
              }
            });
          }
        });
      }
    });(0, _justo.suite)("notRaises()", () => {
      {
        (0, _justo.test)("doesNotRaise()", () => {
          {
            const val = ja(_dogmalangmin.dogma.nop());_chai.assert.strictEqual(val.doesNotRaise(), val);
          }
        });(0, _justo.test)("notRaises()", () => {
          {
            const val = ja(_dogmalangmin.dogma.nop());_chai.assert.strictEqual(val.notRaises(), val);
          }
        });(0, _justo.test)("notRaises() - error when some error raised", () => {
          {
            const val = ja(() => {
              {
                _dogmalangmin.dogma.raise("an error.");
              }
            });_chai.assert.throws(() => {
              {
                val.notRaises();
              }
            });
          }
        });(0, _justo.test)("notRaises() - error when raised error with specified error", () => {
          {
            const val = ja(() => {
              {
                _dogmalangmin.dogma.raise("an error.");
              }
            });_chai.assert.throws(() => {
              {
                val.notRaises("an error.");
              }
            });
          }
        });(0, _justo.test)("notRaises() - when raised error but being another", () => {
          {
            const val = ja(() => {
              {
                _dogmalangmin.dogma.raise("an error.");
              }
            });_chai.assert.strictEqual(val.notRaises("other error."), val);
          }
        });
      }
    });
  }
});