"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

var _chai = require("chai");

const ja = _dogmalangmin.dogma.use(require("../../../../@justojs/assert"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    const val = ja(null);(0, _justo.test)("isNil()", () => {
      {
        _chai.assert.strictEqual(val.isNil(), val);
      }
    });(0, _justo.test)("isNotNil()", () => {
      {
        _chai.assert.throws(() => {
          {
            val.isNotNil();
          }
        });
      }
    });
  }
});