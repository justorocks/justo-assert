"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

var _chai = require("chai");

const ja = _dogmalangmin.dogma.use(require("../../../../@justojs/assert"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.suite)("isXxx()", () => {
      {
        const val = ja((0, _dogmalangmin.set)(1, 2, 3));(0, _justo.test)("isSet()", () => {
          {
            _chai.assert.strictEqual(val.isSet(), val);
          }
        });(0, _justo.test)("isList()", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isList();
              }
            });
          }
        });(0, _justo.test)("isBool() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isBool();
              }
            });
          }
        });(0, _justo.test)("isNum() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNum();
              }
            });
          }
        });(0, _justo.test)("isText() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isText();
              }
            });
          }
        });(0, _justo.test)("isMap()", () => {
          {
            _chai.assert.strictEqual(val.isMap(), val);
          }
        });(0, _justo.test)("isNil() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNil();
              }
            });
          }
        });(0, _justo.test)("isFn() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isFn();
              }
            });
          }
        });(0, _justo.test)("isCallable() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isCallable();
              }
            });
          }
        });
      }
    });(0, _justo.suite)("isNotXxx()", () => {
      {
        const val = ja((0, _dogmalangmin.set)(1, 2, 3));(0, _justo.test)("isNotSet() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNotSet();
              }
            });
          }
        });(0, _justo.test)("isNotList()", () => {
          {
            _chai.assert.strictEqual(val.isNotList(), val);
          }
        });(0, _justo.test)("isBool()", () => {
          {
            _chai.assert.strictEqual(val.isNotBool(), val);
          }
        });(0, _justo.test)("isNum()", () => {
          {
            _chai.assert.strictEqual(val.isNotNum(), val);
          }
        });(0, _justo.test)("isText()", () => {
          {
            _chai.assert.strictEqual(val.isNotText(), val);
          }
        });(0, _justo.test)("isMap() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNotMap();
              }
            });
          }
        });(0, _justo.test)("isNil()", () => {
          {
            _chai.assert.strictEqual(val.isNotNil(), val);
          }
        });(0, _justo.test)("isFn()", () => {
          {
            _chai.assert.strictEqual(val.isNotFn(), val);
          }
        });(0, _justo.test)("isCallable()", () => {
          {
            _chai.assert.strictEqual(val.isNotCallable(), val);
          }
        });
      }
    });
  }
});