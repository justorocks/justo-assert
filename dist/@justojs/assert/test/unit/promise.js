"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

var _chai = require("chai");

const ja = _dogmalangmin.dogma.use(require("../../../../@justojs/assert"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.test)("isPromise()", () => {
      {
        const val = ja(_dogmalangmin.promise.resolve());_chai.assert.strictEqual(val.isPromise(), val);
      }
    });(0, _justo.test)("isPromise() - error", () => {
      {
        const val = ja("hi!");_chai.assert.throws(() => {
          {
            val.isPromise();
          }
        });
      }
    });(0, _justo.test)("isNotPromise() - error", () => {
      {
        const val = ja(_dogmalangmin.promise.resolve());_chai.assert.throws(() => {
          {
            val.isNotPromise();
          }
        });
      }
    });(0, _justo.test)("isNotPromise()", () => {
      {
        const val = ja("hi!");_chai.assert.strictEqual(val.isNotPromise(), val);
      }
    });
  }
});