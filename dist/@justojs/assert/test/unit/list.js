"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

var _chai = require("chai");

const ja = _dogmalangmin.dogma.use(require("../../../../@justojs/assert"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.suite)("isXxx()", () => {
      {
        const val = ja([1, 2, 3]);(0, _justo.test)("isList()", () => {
          {
            _chai.assert.strictEqual(val.isList(), val);
          }
        });(0, _justo.test)("isBool() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isBool();
              }
            });
          }
        });(0, _justo.test)("isNum() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNum();
              }
            });
          }
        });(0, _justo.test)("isText() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isText();
              }
            });
          }
        });(0, _justo.test)("isMap() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isMap();
              }
            });
          }
        });(0, _justo.test)("isNil() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNil();
              }
            });
          }
        });(0, _justo.test)("isFn() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isFn();
              }
            });
          }
        });(0, _justo.test)("isCallable() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isCallable();
              }
            });
          }
        });
      }
    });(0, _justo.suite)("isNotXxx()", () => {
      {
        const val = ja([1, 2, 3]);(0, _justo.test)("isNotList() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.isNotList();
              }
            });
          }
        });(0, _justo.test)("isBool()", () => {
          {
            _chai.assert.strictEqual(val.isNotBool(), val);
          }
        });(0, _justo.test)("isNum()", () => {
          {
            _chai.assert.strictEqual(val.isNotNum(), val);
          }
        });(0, _justo.test)("isText()", () => {
          {
            _chai.assert.strictEqual(val.isNotText(), val);
          }
        });(0, _justo.test)("isMap()", () => {
          {
            _chai.assert.strictEqual(val.isNotMap(), val);
          }
        });(0, _justo.test)("isNil()", () => {
          {
            _chai.assert.strictEqual(val.isNotNil(), val);
          }
        });(0, _justo.test)("isFn()", () => {
          {
            _chai.assert.strictEqual(val.isNotFn(), val);
          }
        });(0, _justo.test)("isCallable()", () => {
          {
            _chai.assert.strictEqual(val.isNotCallable(), val);
          }
        });
      }
    });(0, _justo.suite)("includes()", () => {
      {
        const val = ja([true, "ciao", 12, [1, 2, 3], { ["x"]: 1, ["y"]: 2, ["z"]: 3 }]);(0, _justo.test)("includes(bool)", () => {
          {
            _chai.assert.strictEqual(val.includes(true), val);
          }
        });(0, _justo.test)("includes(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(false);
              }
            });
          }
        });(0, _justo.test)("includes(num)", () => {
          {
            _chai.assert.strictEqual(val.includes(12), val);
          }
        });(0, _justo.test)("includes(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(21);
              }
            });
          }
        });(0, _justo.test)("includes(text)", () => {
          {
            _chai.assert.strictEqual(val.includes("ciao"), val);
          }
        });(0, _justo.test)("includes(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes("c");
              }
            });
          }
        });(0, _justo.test)("includes(list)", () => {
          {
            _chai.assert.strictEqual(val.includes([[1, 2, 3]]), val);
          }
        });(0, _justo.test)("includes(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes([[3, 2, 1]]);
              }
            });
          }
        });(0, _justo.test)("includes(map)", () => {
          {
            _chai.assert.strictEqual(val.includes({ ["x"]: 1, ["y"]: 2, ["z"]: 3 }), val);
          }
        });(0, _justo.test)("includes(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes({ ["x"]: 1 });
              }
            });
          }
        });
      }
    });(0, _justo.suite)("notIncludes()", () => {
      {
        const val = ja([true, "ciao", 12, [1, 2, 3], { ["x"]: 1, ["y"]: 2, ["z"]: 3 }]);(0, _justo.test)("notIncludes(bool)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(false), val);
          }
        });(0, _justo.test)("notIncludes(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notIncludes(true);
              }
            });
          }
        });(0, _justo.test)("notIncludes(num)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(21), val);
          }
        });(0, _justo.test)("notIncludes(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notIncludes(12);
              }
            });
          }
        });(0, _justo.test)("notIncludes(text)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes("oaic"), val);
          }
        });(0, _justo.test)("notIncludes(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notIncludes("ciao");
              }
            });
          }
        });(0, _justo.test)("notIncludes(list)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes([[3, 2, 1]]), val);
          }
        });(0, _justo.test)("notIncludes(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notIncludes([[1, 2, 3]]);
              }
            });
          }
        });(0, _justo.test)("notIncludes(map)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes({ ["x"]: 1 }), val);
          }
        });(0, _justo.test)("notIncludes(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notIncludes({ ["x"]: 1, ["y"]: 2, ["z"]: 3 });
              }
            });
          }
        });
      }
    });(0, _justo.suite)("isEmpty()", () => {
      {
        (0, _justo.test)("isEmpty()", () => {
          {
            const val = ja([]);_chai.assert.strictEqual(val.isEmpty(), val);
          }
        });(0, _justo.test)("isEmpty() - error", () => {
          {
            const val = ja([1, 2, 3]);_chai.assert.throws(() => {
              {
                val.isEmpty();
              }
            });
          }
        });
      }
    });(0, _justo.suite)("isNotEmpty()", () => {
      {
        (0, _justo.test)("isNotEmpty()", () => {
          {
            const val = ja([1, 2, 3]);_chai.assert.strictEqual(val.isNotEmpty(), val);
          }
        });(0, _justo.test)("isNotEmpty() - error", () => {
          {
            const val = ja([]);_chai.assert.throws(() => {
              {
                val.isNotEmpty();
              }
            });
          }
        });
      }
    });(0, _justo.suite)("len()", () => {
      {
        const val = ja([1, 2, 3]);(0, _justo.test)("len(num)", () => {
          {
            _chai.assert.strictEqual(val.len(3), val);
          }
        });(0, _justo.test)("len(list)", () => {
          {
            _chai.assert.strictEqual(val.len([1, 3, 5]), val);
          }
        });(0, _justo.test)("len() - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.len(2);
              }
            });
          }
        });
      }
    });(0, _justo.suite)("similarTo()", () => {
      {
        let val = ja([1, 2, 3]);(0, _justo.test)("similarTo(list)", () => {
          {
            _chai.assert.strictEqual(val.similarTo([1, 3, 2]), val);
          }
        });(0, _justo.test)("similarTo(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.similarTo([1, 3, 4]);
              }
            });
          }
        });(0, _justo.test)("similarTo(list) - error by different len", () => {
          {
            _chai.assert.throws(() => {
              {
                val.similarTo([1, 3]);
              }
            });
          }
        });(0, _justo.test)("[].similarTo(list)", () => {
          {
            val = ja([]);_chai.assert.strictEqual(val.similarTo([]), val);
          }
        });
      }
    });(0, _justo.suite)("notSimilarTo()", () => {
      {
        const val = ja([1, 2, 3]);(0, _justo.test)("notsimilarTo(list)", () => {
          {
            _chai.assert.strictEqual(val.notSimilarTo([1, 3, 5]), val);
          }
        });(0, _justo.test)("notsimilar(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notSimilarTo([1, 3, 2]);
              }
            });
          }
        });
      }
    });(0, _justo.suite)("forEach()", () => {
      {
        (0, _justo.suite)("forEach(map)", () => {
          {
            const val = ja([{ ["a"]: 1, ["b"]: 1 }, { ["a"]: 1, ["b"]: 2 }, { ["a"]: 1, ["c"]: 3 }]);(0, _justo.test)("ok", () => {
              {
                _chai.assert.strictEqual(val.forEach({ ["a"]: 1 }), val);
              }
            });(0, _justo.test)("error", () => {
              {
                _chai.assert.throws(() => {
                  {
                    val.forEach({ ["b"]: 1 });
                  }
                });
              }
            });
          }
        });(0, _justo.suite)("forEach(func)", () => {
          {
            const val = ja([1, 2, 3, 4]);(0, _justo.test)("ok", () => {
              {
                _chai.assert.strictEqual(val.forEach(i => {
                  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("i", i, null);{
                    _chai.assert.strictEqual(_dogmalangmin.dogma.is(i, _dogmalangmin.num), true);
                  }
                }), val);
              }
            });(0, _justo.test)("error", () => {
              {
                _chai.assert.throws(() => {
                  {
                    val.forEach(i => {
                      /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("i", i, null);{
                        _chai.assert.strictEqual(_dogmalangmin.dogma.is(i, _dogmalangmin.text), true);
                      }
                    });
                  }
                }, "expected false to equal true");
              }
            });
          }
        });
      }
    });
  }
});