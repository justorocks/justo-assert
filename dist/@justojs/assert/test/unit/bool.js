"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

var _chai = require("chai");

const ja = _dogmalangmin.dogma.use(require("../../../../@justojs/assert"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.test)("isBool()", params => {
      /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("params", params, null);{
        const val = ja(true);_chai.assert.strictEqual((0, _dogmalangmin.bind)(val, params.meth)(), val);
      }
    }).forEach({ ["sub"]: "isBool()", ["meth"]: "isBool" }, { ["sub"]: "isBoolean", ["meth"]: "isBoolean" });(0, _justo.test)("isNotBool() - error", params => {
      /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("params", params, null);{
        const val = ja(true);_chai.assert.throws(() => {
          {
            (0, _dogmalangmin.bind)(val, params.meth)();
          }
        });
      }
    }).forEach({ ["sub"]: "isNotBool()", ["meth"]: "isNotBool" }, { ["sub"]: "isNotBoolean()", ["meth"]: "isNotBoolean" });(0, _justo.test)("isText() - error", () => {
      {
        const val = ja(true);_chai.assert.throws(() => {
          {
            val.isText();
          }
        });
      }
    });(0, _justo.test)("isNotText()", () => {
      {
        const val = ja(true);_chai.assert.strictEqual(val.isNotText(), val);
      }
    });(0, _justo.test)("isNum() - error", () => {
      {
        const val = ja(true);_chai.assert.throws(() => {
          {
            val.isNum();
          }
        });
      }
    });(0, _justo.test)("isNotNum()", () => {
      {
        const val = ja(true);_chai.assert.strictEqual(val.isNotNum(), val);
      }
    });(0, _justo.test)("isList() - error", () => {
      {
        const val = ja(true);_chai.assert.throws(() => {
          {
            val.isList();
          }
        });
      }
    });(0, _justo.test)("isNotList()", () => {
      {
        const val = ja(true);_chai.assert.strictEqual(val.isNotList(), val);
      }
    });(0, _justo.test)("isSet() - error", () => {
      {
        const val = ja(true);_chai.assert.throws(() => {
          {
            val.isSet();
          }
        });
      }
    });(0, _justo.test)("isNotSet()", () => {
      {
        const val = ja(true);_chai.assert.strictEqual(val.isNotSet(), val);
      }
    });(0, _justo.test)("isMap() - error", () => {
      {
        const val = ja(true);_chai.assert.throws(() => {
          {
            val.isMap();
          }
        });
      }
    });(0, _justo.test)("isNotMap()", () => {
      {
        const val = ja(true);_chai.assert.strictEqual(val.isNotMap(), val);
      }
    });(0, _justo.test)("isFn() - error", () => {
      {
        const val = ja(true);_chai.assert.throws(() => {
          {
            val.isFn();
          }
        });
      }
    });(0, _justo.test)("isNotFn()", () => {
      {
        const val = ja(true);_chai.assert.strictEqual(val.isNotFn(), val);
      }
    });(0, _justo.test)("isCallable() - error", () => {
      {
        const val = ja(true);_chai.assert.throws(() => {
          {
            val.isCallable();
          }
        });
      }
    });(0, _justo.test)("isNotCallable()", () => {
      {
        const val = ja(true);_chai.assert.strictEqual(val.isNotCallable(), val);
      }
    });(0, _justo.suite)("eq()", () => {
      {
        (0, _justo.test)("eq() - true equal to true", () => {
          {
            const val = ja(true);_chai.assert.strictEqual(val.eq(true), val);
          }
        });(0, _justo.test)("eq() - error when true not equal to false", () => {
          {
            const val = ja(true);_chai.assert.throws(() => {
              {
                val.eq(false);
              }
            });
          }
        });(0, _justo.test)("eq() - error when text", () => {
          {
            const val = ja(true);_chai.assert.throws(() => {
              {
                val.eq("true");
              }
            });
          }
        });(0, _justo.test)("eq() - error when num", () => {
          {
            const val = ja(false);_chai.assert.throws(() => {
              {
                val.eq([]);
              }
            });
          }
        });(0, _justo.test)("eq() - error when map", () => {
          {
            const val = ja(false);_chai.assert.throws(() => {
              {
                val.eq({});
              }
            });
          }
        });(0, _justo.test)("eq() - error when fn", () => {
          {
            const val = ja(false);_chai.assert.throws(() => {
              {
                val.eq(_dogmalangmin.dogma.nop());
              }
            });
          }
        });
      }
    });(0, _justo.suite)("ne()", () => {
      {
        (0, _justo.test)("ne() - true not equal to false", () => {
          {
            const val = ja(true);_chai.assert.strictEqual(val.ne(false), val);
          }
        });(0, _justo.test)("ne() - error when true not equal to true", () => {
          {
            const val = ja(true);_chai.assert.throws(() => {
              {
                val.ne(true);
              }
            });
          }
        });(0, _justo.test)("ne() - when text", () => {
          {
            const val = ja(true);_chai.assert.strictEqual(val.ne("true"), val);
          }
        });(0, _justo.test)("ne() - when num", () => {
          {
            const val = ja(false);_chai.assert.strictEqual(val.ne([]), val);
          }
        });(0, _justo.test)("ne() - when map", () => {
          {
            const val = ja(false);_chai.assert.strictEqual(val.ne({}), val);
          }
        });(0, _justo.test)("ne() - when fn", () => {
          {
            const val = ja(false);_chai.assert.strictEqual(val.ne(_dogmalangmin.dogma.nop()), val);
          }
        });
      }
    });(0, _justo.suite)("sameAs()", () => {
      {
        const val = ja(true);(0, _justo.test)("sameAs()", () => {
          {
            _chai.assert.strictEqual(val.sameAs(true), val);
          }
        });(0, _justo.test)("sameAs() - error with bool", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs(false);
              }
            });
          }
        });(0, _justo.test)("sameAs() - error with text", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs("true");
              }
            });
          }
        });(0, _justo.test)("sameAs() - error with num", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs(1);
              }
            });
          }
        });(0, _justo.test)("sameAs() - error with list", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs([1, 2, 3]);
              }
            });
          }
        });(0, _justo.test)("sameAs() - error with map", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs({ ["x"]: 1 });
              }
            });
          }
        });(0, _justo.test)("sameAs() - error with fn", () => {
          {
            _chai.assert.throws(() => {
              {
                val.sameAs(_dogmalangmin.dogma.nop());
              }
            });
          }
        });
      }
    });(0, _justo.suite)("notSameAs()", () => {
      {
        const val = ja(true);(0, _justo.test)("notSameAs()", () => {
          {
            _chai.assert.strictEqual(val.notSameAs(false), val);
          }
        });(0, _justo.test)("notSameAs() - error with bool", () => {
          {
            _chai.assert.throws(() => {
              {
                val.notSameAs(true);
              }
            });
          }
        });(0, _justo.test)("notSameAs() - with text", () => {
          {
            _chai.assert.strictEqual(val.notSameAs("true"), val);
          }
        });(0, _justo.test)("notSameAs() - with num", () => {
          {
            _chai.assert.strictEqual(val.notSameAs(1), val);
          }
        });(0, _justo.test)("notSameAs() - with list", () => {
          {
            _chai.assert.strictEqual(val.notSameAs([1, 2, 3]), val);
          }
        });(0, _justo.test)("notSameAs() - with map", () => {
          {
            _chai.assert.strictEqual(val.notSameAs({ ["x"]: 1 }), val);
          }
        });(0, _justo.test)("notSameAs() - with fn", () => {
          {
            _chai.assert.strictEqual(val.notSameAs(_dogmalangmin.dogma.nop()), val);
          }
        });
      }
    });(0, _justo.suite)("lt()", () => {
      {
        const val = ja(true);(0, _justo.test)("lt(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt(false);
              }
            });
          }
        });(0, _justo.test)("lt(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt("true");
              }
            });
          }
        });(0, _justo.test)("lt(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt(1);
              }
            });
          }
        });(0, _justo.test)("lt(nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt(null);
              }
            });
          }
        });(0, _justo.test)("lt(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt([]);
              }
            });
          }
        });(0, _justo.test)("lt(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt({});
              }
            });
          }
        });(0, _justo.test)("lt(fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.lt(_dogmalangmin.dogma.nop());
              }
            });
          }
        });
      }
    });(0, _justo.suite)("le()", () => {
      {
        const val = ja(true);(0, _justo.test)("le(bool)", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le(false);
              }
            });
          }
        });(0, _justo.test)("le(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le("true");
              }
            });
          }
        });(0, _justo.test)("le(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le(1);
              }
            });
          }
        });(0, _justo.test)("le(nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le(null);
              }
            });
          }
        });(0, _justo.test)("le(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le([]);
              }
            });
          }
        });(0, _justo.test)("le(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le({});
              }
            });
          }
        });(0, _justo.test)("le(fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.le(_dogmalangmin.dogma.nop());
              }
            });
          }
        });
      }
    });(0, _justo.suite)("gt()", () => {
      {
        const val = ja(true);(0, _justo.test)("gt(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt(false);
              }
            });
          }
        });(0, _justo.test)("gt(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt("true");
              }
            });
          }
        });(0, _justo.test)("gt(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt(1);
              }
            });
          }
        });(0, _justo.test)("gt(nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt(null);
              }
            });
          }
        });(0, _justo.test)("gt(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt([]);
              }
            });
          }
        });(0, _justo.test)("gt(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt({});
              }
            });
          }
        });(0, _justo.test)("gt(fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.gt(_dogmalangmin.dogma.nop());
              }
            });
          }
        });
      }
    });(0, _justo.suite)("ge()", () => {
      {
        const val = ja(true);(0, _justo.test)("ge(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge(false);
              }
            });
          }
        });(0, _justo.test)("ge(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge("true");
              }
            });
          }
        });(0, _justo.test)("ge(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge(1);
              }
            });
          }
        });(0, _justo.test)("ge(nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge(null);
              }
            });
          }
        });(0, _justo.test)("ge(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge([]);
              }
            });
          }
        });(0, _justo.test)("ge(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge({});
              }
            });
          }
        });(0, _justo.test)("ge(fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.ge(_dogmalangmin.dogma.nop());
              }
            });
          }
        });
      }
    });(0, _justo.suite)("between()", () => {
      {
        const val = ja(true);(0, _justo.test)("between(bool, bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between(true, false);
              }
            });
          }
        });(0, _justo.test)("between(text, text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between("true", "false");
              }
            });
          }
        });(0, _justo.test)("between(num, num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between(0, 1);
              }
            });
          }
        });(0, _justo.test)("between(list, list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between([], [true, false]);
              }
            });
          }
        });(0, _justo.test)("between(map, map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between({}, { ["x"]: 1, ["y"]: 2 });
              }
            });
          }
        });(0, _justo.test)("between(nil, nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between(null, null);
              }
            });
          }
        });(0, _justo.test)("between(fn, fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.between(_dogmalangmin.dogma.nop(), _dogmalangmin.dogma.nop());
              }
            });
          }
        });
      }
    });(0, _justo.suite)("notBetween()", () => {
      {
        const val = ja(true);(0, _justo.test)("notBetween(bool, bool)", () => {
          {
            _chai.assert.strictEqual(val.notBetween(true, false), val);
          }
        });(0, _justo.test)("notBetween(text, text)", () => {
          {
            _chai.assert.strictEqual(val.notBetween("true", "false"), val);
          }
        });(0, _justo.test)("notBetween(num, num)", () => {
          {
            _chai.assert.strictEqual(val.notBetween(0, 1), val);
          }
        });(0, _justo.test)("notBetween(list, list)", () => {
          {
            _chai.assert.strictEqual(val.notBetween([], [true, false]), val);
          }
        });(0, _justo.test)("notBetween(map, map)", () => {
          {
            _chai.assert.strictEqual(val.notBetween({}, { ["x"]: 1, ["y"]: 2 }), val);
          }
        });(0, _justo.test)("notBetween(nil, nil)", () => {
          {
            _chai.assert.strictEqual(val.notBetween(null, null), val);
          }
        });(0, _justo.test)("notBetween(fn, fn)", () => {
          {
            _chai.assert.strictEqual(val.notBetween(_dogmalangmin.dogma.nop(), _dogmalangmin.dogma.nop()), val);
          }
        });
      }
    });(0, _justo.suite)("includes()", () => {
      {
        const val = ja(true);(0, _justo.test)("includes(bool) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(true);
              }
            });
          }
        });(0, _justo.test)("includes(text) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes("true");
              }
            });
          }
        });(0, _justo.test)("includes(num) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(123);
              }
            });
          }
        });(0, _justo.test)("includes(list) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes([true]);
              }
            });
          }
        });(0, _justo.test)("includes(map) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes({ ["x"]: true });
              }
            });
          }
        });(0, _justo.test)("includes(nil) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(null);
              }
            });
          }
        });(0, _justo.test)("includes(fn) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.includes(_dogmalangmin.dogma.nop());
              }
            });
          }
        });
      }
    });(0, _justo.suite)("notIncludes()", () => {
      {
        const val = ja(true);(0, _justo.test)("notIncludes(bool)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(true), val);
          }
        });(0, _justo.test)("notIncludes(text)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes("true"), val);
          }
        });(0, _justo.test)("notIncludes(num)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(123), val);
          }
        });(0, _justo.test)("notIncludes(list)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes([true]), val);
          }
        });(0, _justo.test)("notIncludes(map)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes({ ["x"]: true }), val);
          }
        });(0, _justo.test)("notIncludes(nil)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(null), val);
          }
        });(0, _justo.test)("notIncludes(fn)", () => {
          {
            _chai.assert.strictEqual(val.notIncludes(_dogmalangmin.dogma.nop()), val);
          }
        });
      }
    });(0, _justo.suite)("has()", () => {
      {
        const val = ja(true);(0, _justo.test)("has(mem) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.has("xyz");
              }
            });
          }
        });(0, _justo.test)("has(mems) - error", () => {
          {
            _chai.assert.throws(() => {
              {
                val.has(["abc", "xyz"]);
              }
            });
          }
        });
      }
    });(0, _justo.suite)("notHas()", () => {
      {
        const val = ja(true);(0, _justo.test)("notHas(mem)", () => {
          {
            _chai.assert.strictEqual(val.notHas("xyz"), val);
          }
        });(0, _justo.test)("notHas(mems)", () => {
          {
            _chai.assert.strictEqual(val.notHas(["abc", "xyz"]), val);
          }
        });
      }
    });(0, _justo.test)("isEmpty() - error", () => {
      {
        const val = ja(true);_chai.assert.throws(() => {
          {
            val.isEmpty();
          }
        });
      }
    });(0, _justo.test)("isNotEmpty() - error", () => {
      {
        const val = ja(true);_chai.assert.throws(() => {
          {
            val.isNotEmpty();
          }
        });
      }
    });(0, _justo.test)("len() - error", () => {
      {
        const val = ja(true);_chai.assert.throws(() => {
          {
            val.len(0);
          }
        });
      }
    });(0, _justo.test)("notLen() - error", () => {
      {
        const val = ja(true);_chai.assert.throws(() => {
          {
            val.notLen(0);
          }
        });
      }
    });
  }
});