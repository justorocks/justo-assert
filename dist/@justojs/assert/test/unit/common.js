"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

var _chai = require("chai");

const ja = _dogmalangmin.dogma.use(require("../../../../@justojs/assert"));
const $Test = class Test {
  constructor() {
    {}
  }
};
const Test = new Proxy($Test, { apply(receiver, self, args) {
    return new $Test(...args);
  } });
const $Test2 = class Test2 {
  constructor() {
    {}
  }
};
const Test2 = new Proxy($Test2, { apply(receiver, self, args) {
    return new $Test2(...args);
  } });
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.suite)("mem()", () => {
      {
        (0, _justo.test)("map", () => {
          {
            const val = ja({ ["x"]: 123, ["y"]: 456 });_chai.assert.strictEqual(val.mem("x").value, 123);_chai.assert.strictEqual(val.mem("y").value, 456);_chai.assert.strictEqual(val.mem("x").mem("y").value, 456);_chai.assert.strictEqual(val.mem("x").mem("y").mem("x").value, 123);_chai.assert.equal(val.mem("z").value, null);
          }
        });(0, _justo.test)("list, using alias item()", () => {
          {
            const val = ja([123, 456]);_chai.assert.strictEqual(val.item(0).value, 123);_chai.assert.strictEqual(val.item(1).value, 456);_chai.assert.equal(val.item(2).value, null);
          }
        });(0, _justo.test)("mem(field, index)", () => {
          {
            const val = ja({ ["x"]: ["zero", "one", "two", "three"], ["y"]: 456 });_chai.assert.strictEqual(val.mem("x", 0).value, "zero");_chai.assert.strictEqual(val.mem("x", 1).value, "one");_chai.assert.equal(val.mem("x", 123).value, null);
          }
        });
      }
    });(0, _justo.suite)("isInstanceOf()", () => {
      {
        (0, _justo.test)("is(class:text)", () => {
          {
            const val = ja(Test());_chai.assert.strictEqual(val.is("Test"), val);
          }
        });(0, _justo.test)("isInstanceOf(class:text)", () => {
          {
            const val = ja(Test());_chai.assert.strictEqual(val.isInstanceOf("Test"), val);
          }
        });(0, _justo.test)("isInstanceOf(class:text) - error", () => {
          {
            const val = ja(Test());_chai.assert.throws(() => {
              {
                val.isInstanceOf("Test2");
              }
            });
          }
        });(0, _justo.test)("isInstanceof(class:Class)", () => {
          {
            const val = ja(Test());_chai.assert.strictEqual(val.isInstanceOf(Test), val);
          }
        });(0, _justo.test)("isInstanceof(class:Class) - error", () => {
          {
            const val = ja(Test());_chai.assert.throws(() => {
              {
                val.isInstanceOf(Test2);
              }
            });
          }
        });(0, _justo.test)("null.isInstanceOf(class:text) - error", () => {
          {
            const val = ja(null);_chai.assert.throws(() => {
              {
                val.isInstanceOf("Test");
              }
            });
          }
        });(0, _justo.test)("undefined.isInstanceOf(class:text) - error", () => {
          {
            const val = ja(undefined);_chai.assert.throws(() => {
              {
                val.isInstanceOf("Test");
              }
            });
          }
        });
      }
    });(0, _justo.suite)("isNotInstanceOf()", () => {
      {
        (0, _justo.test)("isNot(class:text)", () => {
          {
            const val = ja(Test());_chai.assert.strictEqual(val.isNot("Test2"), val);
          }
        });(0, _justo.test)("isNotInstanceOf(class:text)", () => {
          {
            const val = ja(Test());_chai.assert.strictEqual(val.isNotInstanceOf("Test2"), val);
          }
        });(0, _justo.test)("isNotInstanceOf(class:text) - error", () => {
          {
            const val = ja(Test());_chai.assert.throws(() => {
              {
                val.isNotInstanceOf("Test");
              }
            });
          }
        });(0, _justo.test)("isNotInstanceof(class:Class)", () => {
          {
            const val = ja(Test());_chai.assert.strictEqual(val.isNotInstanceOf(Test2), val);
          }
        });(0, _justo.test)("isNotInstanceof(class:Class) - error", () => {
          {
            const val = ja(Test());_chai.assert.throws(() => {
              {
                val.isNotInstanceOf(Test);
              }
            });
          }
        });(0, _justo.test)("null.isNotInstanceOf(class:text)", () => {
          {
            const val = ja(null);_chai.assert.strictEqual(val.isNotInstanceOf("Test"), val);
          }
        });(0, _justo.test)("undefined.isNotInstanceOf(class:text) - error", () => {
          {
            const val = ja(undefined);_chai.assert.strictEqual(val.isNotInstanceOf("Test"), val);
          }
        });
      }
    });(0, _justo.suite)("isCallable() - error", () => {
      {
        const val = ja("text");_chai.assert.throws(() => {
          {
            val.isCallable();
          }
        });
      }
    });(0, _justo.suite)("isNum() - error", () => {
      {
        const val = ja("123");_chai.assert.throws(() => {
          {
            val.isNum();
          }
        });
      }
    });(0, _justo.suite)("isNotNil()", () => {
      {
        const val = ja("bonjour");_chai.assert.strictEqual(val.isNotNil(), val);
      }
    });
  }
});