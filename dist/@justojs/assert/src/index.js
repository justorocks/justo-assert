"use strict";

var _dogmalangmin = require("dogmalangmin");

const ValueWrapper = _dogmalangmin.dogma.use(require("./ValueWrapper"));
function assert(...args) {
  {
    return ValueWrapper(...args);
  }
}module.exports = exports = assert;