"use strict";

var _dogmalangmin = require("dogmalangmin");

const assert = _dogmalangmin.dogma.use(require("assert"));
const $ValueWrapper = class ValueWrapper {
  constructor(value, object = null) {
    Object.defineProperty(this, "value", { value: value, enum: true, writable: false });Object.defineProperty(this, "object", { value: object, enum: true, writable: false });{}
  }
};
const ValueWrapper = new Proxy($ValueWrapper, { apply(receiver, self, args) {
    return new $ValueWrapper(...args);
  } });module.exports = exports = ValueWrapper;
ValueWrapper.prototype.item = ValueWrapper.prototype.mem = function (...args) {
  {
    const root = (0, _dogmalangmin.coalesce)(this.object, this.value);let item = root;for (const i of args) {
      item = _dogmalangmin.dogma.getItem(item, i);
    }return ValueWrapper(item, root);
  }
};
ValueWrapper.prototype.raises = function (err) {
  {
    {
      const [ok, res] = _dogmalangmin.dogma.peval(() => {
        return this.value();
      });if (ok) {
        _dogmalangmin.dogma.raise("function must raise error.");
      } else if (err && res != err) {
        _dogmalangmin.dogma.raise("function must raise '%s'. Raised: '%s'.", err, res);
      }
    }
  }return this;
};
ValueWrapper.prototype.doesNotRaise = ValueWrapper.prototype.notRaises = function (err) {
  {
    {
      const [ok, res] = _dogmalangmin.dogma.peval(() => {
        return this.value();
      });if (!ok) {
        if (err) {
          if (res == err) {
            _dogmalangmin.dogma.raise("function must not raise '%s'.", err);
          }
        } else {
          _dogmalangmin.dogma.raise("function must not raise error.");
        }
      }
    }
  }return this;
};
ValueWrapper.prototype.is = ValueWrapper.prototype.isInstanceOf = function (class_) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("class_", class_, null);{
    if (this.value != null) {
      if (_dogmalangmin.dogma.is(class_, _dogmalangmin.text)) {
        if (this.value.constructor.name != class_) {
          _dogmalangmin.dogma.raise("'%s' must be instance of '%s'.", this.value, class_);
        }
      } else {
        if (_dogmalangmin.dogma.isNot(this.value, class_)) {
          _dogmalangmin.dogma.raise("'%s' must be instance of '%s'.", this.value, class_);
        }
      }
    } else {
      _dogmalangmin.dogma.raise("'%s' must be instance of '%s'.", this.value === null ? "null" : "undefined", class_);
    }
  }return this;
};
ValueWrapper.prototype.isNot = ValueWrapper.prototype.isNotInstanceOf = function (class_) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("class_", class_, null);{
    if (this.value != null) {
      if (_dogmalangmin.dogma.is(class_, _dogmalangmin.text)) {
        if (this.value.constructor.name == class_) {
          _dogmalangmin.dogma.raise("'%s' must not be instance of '%s'.", this.value, class_);
        }
      } else {
        if (_dogmalangmin.dogma.is(this.value, class_)) {
          _dogmalangmin.dogma.raise("'%s' must not be instance of '%s'.", this.value, class_);
        }
      }
    }
  }return this;
};
ValueWrapper.prototype.isNull = ValueWrapper.prototype.isNil = function () {
  {
    if (this.value != null) {
      _dogmalangmin.dogma.raise("'%s' must be nil/null.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isNotNull = ValueWrapper.prototype.isNotNil = function () {
  {
    if (this.value == null) {
      _dogmalangmin.dogma.raise("'%s' must not be nil/null.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isBoolean = ValueWrapper.prototype.isBool = function () {
  {
    if (_dogmalangmin.dogma.isNot(this.value, _dogmalangmin.bool)) {
      _dogmalangmin.dogma.raise("'%s' must be boolean.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isNotBoolean = ValueWrapper.prototype.isNotBool = function () {
  {
    if (_dogmalangmin.dogma.is(this.value, _dogmalangmin.bool)) {
      _dogmalangmin.dogma.raise("'%s' must not be boolean.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isString = ValueWrapper.prototype.isText = function () {
  {
    if (_dogmalangmin.dogma.isNot(this.value, _dogmalangmin.text)) {
      _dogmalangmin.dogma.raise("'%s' must be text or string.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isNotString = ValueWrapper.prototype.isNotText = function () {
  {
    if (_dogmalangmin.dogma.is(this.value, _dogmalangmin.text)) {
      _dogmalangmin.dogma.raise("'%s' must not be text or string.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isNumber = ValueWrapper.prototype.isNum = function () {
  {
    if (_dogmalangmin.dogma.isNot(this.value, _dogmalangmin.num)) {
      _dogmalangmin.dogma.raise("'%s' must be number.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isNotNumber = ValueWrapper.prototype.isNotNum = function () {
  {
    if (_dogmalangmin.dogma.is(this.value, _dogmalangmin.num)) {
      _dogmalangmin.dogma.raise("'%s' must not be number.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isArray = ValueWrapper.prototype.isList = function () {
  {
    if (_dogmalangmin.dogma.isNot(this.value, _dogmalangmin.list)) {
      _dogmalangmin.dogma.raise("'%s' must be list or array.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isNotArray = ValueWrapper.prototype.isNotList = function () {
  {
    if (_dogmalangmin.dogma.is(this.value, _dogmalangmin.list)) {
      _dogmalangmin.dogma.raise("'%s' must not be list or array.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isSet = function () {
  {
    if (_dogmalangmin.dogma.isNot(this.value, _dogmalangmin.set)) {
      _dogmalangmin.dogma.raise("'%s' must be set.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isNotSet = function () {
  {
    if (_dogmalangmin.dogma.is(this.value, _dogmalangmin.set)) {
      _dogmalangmin.dogma.raise("'%s' must not be set.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isObject = ValueWrapper.prototype.isMap = function () {
  {
    if (_dogmalangmin.dogma.isNot(this.value, _dogmalangmin.map)) {
      _dogmalangmin.dogma.raise("'%s' must not map or object.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isNotObject = ValueWrapper.prototype.isNotMap = function () {
  {
    if (_dogmalangmin.dogma.is(this.value, _dogmalangmin.map)) {
      _dogmalangmin.dogma.raise("'%s' must not map or object.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isFunction = ValueWrapper.prototype.isFn = function () {
  {
    if (_dogmalangmin.dogma.isNot(this.value, _dogmalangmin.func)) {
      _dogmalangmin.dogma.raise("'%s' must be function.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isNotFunction = ValueWrapper.prototype.isNotFn = function () {
  {
    if (_dogmalangmin.dogma.is(this.value, _dogmalangmin.func)) {
      _dogmalangmin.dogma.raise("'%s' must not be function.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isPromise = function () {
  {
    if (_dogmalangmin.dogma.isNot(this.value, _dogmalangmin.promise)) {
      _dogmalangmin.dogma.raise("'%s' must be promise.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isNotPromise = function () {
  {
    if (_dogmalangmin.dogma.is(this.value, _dogmalangmin.promise)) {
      _dogmalangmin.dogma.raise("'%s' must not be promise.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isCallable = function () {
  {
    if (_dogmalangmin.dogma.isNot(this.value, _dogmalangmin.func)) {
      _dogmalangmin.dogma.raise("'%s' must be callable.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.isNotCallable = function () {
  {
    if (_dogmalangmin.dogma.is(this.value, _dogmalangmin.func)) {
      _dogmalangmin.dogma.raise("'%s' must not be callable.", this.value);
    }
  }return this;
};
ValueWrapper.prototype.eq = function (val) {
  {
    if ((0, _dogmalangmin.typename)(this.value) != (0, _dogmalangmin.typename)(val) || !_dogmalangmin.dogma.getItem(_dogmalangmin.dogma.peval(() => {
      return assert.deepEqual(this.value, val);
    }), 0)) {
      _dogmalangmin.dogma.raise("%s must be equal to %s.", (0, _dogmalangmin.fmt)(this.value), (0, _dogmalangmin.fmt)(val));
    }
  }return this;
};
ValueWrapper.prototype.neq = ValueWrapper.prototype.ne = function (val) {
  {
    if ((0, _dogmalangmin.typename)(this.value) == (0, _dogmalangmin.typename)(val) && _dogmalangmin.dogma.getItem(_dogmalangmin.dogma.peval(() => {
      return assert.deepEqual(this.value, val);
    }), 0)) {
      _dogmalangmin.dogma.raise("%s must not be equal to %s.", (0, _dogmalangmin.fmt)(this.value), (0, _dogmalangmin.fmt)(val));
    }
  }return this;
};
ValueWrapper.prototype.sameAs = function (val) {
  {
    if (this.value !== val) {
      _dogmalangmin.dogma.raise("%s must be same as %s.", (0, _dogmalangmin.fmt)(this.value), (0, _dogmalangmin.fmt)(val));
    }
  }return this;
};
ValueWrapper.prototype.notSameAs = function (val) {
  {
    if (this.value === val) {
      _dogmalangmin.dogma.raise("%s must not be same as %s.", (0, _dogmalangmin.fmt)(this.value), (0, _dogmalangmin.fmt)(val));
    }
  }return this;
};
ValueWrapper.prototype.lt = function (val) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("val", val, null);{
    if ((0, _dogmalangmin.typename)(this.value) != (0, _dogmalangmin.typename)(val) || this.value >= val) {
      _dogmalangmin.dogma.raise("'%s' must be less than '%s'.", this.value, val);
    }
  }return this;
};
ValueWrapper.prototype.le = function (val) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("val", val, null);{
    if ((0, _dogmalangmin.typename)(this.value) != (0, _dogmalangmin.typename)(val) || this.value > val) {
      _dogmalangmin.dogma.raise("'%s' must be less than or equal to '%s'.", this.value, val);
    }
  }return this;
};
ValueWrapper.prototype.gt = function (val) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("val", val, null);{
    if ((0, _dogmalangmin.typename)(this.value) != (0, _dogmalangmin.typename)(val) || _dogmalangmin.dogma.includes(["bool"], (0, _dogmalangmin.typename)(this.value)) || this.value <= val) {
      _dogmalangmin.dogma.raise("'%s' must be greater than '%s'.", this.value, val);
    }
  }return this;
};
ValueWrapper.prototype.ge = function (val) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("val", val, null);{
    if ((0, _dogmalangmin.typename)(this.value) != (0, _dogmalangmin.typename)(val) || _dogmalangmin.dogma.includes(["bool"], (0, _dogmalangmin.typename)(this.value)) || this.value < val) {
      _dogmalangmin.dogma.raise("'%s' must be greater than or equal to '%s'.", this.value, val);
    }
  }return this;
};
ValueWrapper.prototype.between = function (val1, val2) {
  {
    if ((0, _dogmalangmin.typename)(this.value) != (0, _dogmalangmin.typename)(val1) || (0, _dogmalangmin.typename)(this.value) != (0, _dogmalangmin.typename)(val2) || this.value < val1 || this.value > val2) {
      _dogmalangmin.dogma.raise("'%s' must be between '%s' and '%s'.", this.value, val1, val2);
    }
  }return this;
};
ValueWrapper.prototype.notBetween = function (val1, val2) {
  {
    if ((0, _dogmalangmin.typename)(this.value) == (0, _dogmalangmin.typename)(val1) && (0, _dogmalangmin.typename)(this.value) == (0, _dogmalangmin.typename)(val2) && this.value >= val1 && this.value <= val2) {
      _dogmalangmin.dogma.raise("'%s' must not be between '%s' and '%s'.", this.value, val1, val2);
    }
  }return this;
};
ValueWrapper.prototype.includes = function (vals) {
  vals = (0, _dogmalangmin.list)(vals);{
    if ((0, _dogmalangmin.len)(vals) == 0) {
      _dogmalangmin.dogma.raise("'%s' must include something non-indicated.", this.value);
    }for (const val of vals) {
      if (_dogmalangmin.dogma.is(this.value, _dogmalangmin.list) && !_dogmalangmin.dogma.includes(["bool", "num", "text", "nil"], (0, _dogmalangmin.typename)(val))) {
        let exists = false;for (const item of this.value) {
          [exists] = _dogmalangmin.dogma.getArrayToUnpack(_dogmalangmin.dogma.peval(() => {
            return assert.deepEqual(item, val);
          }), 1);if (exists) {
            break;
          }
        }if (!exists) {
          _dogmalangmin.dogma.raise("'%s' must include '%s'.", this.value, val);
        }
      } else {
        {
          const [ok, res] = _dogmalangmin.dogma.peval(() => {
            return this.value.includes(val);
          });if (!ok || !res) {
            _dogmalangmin.dogma.raise("'%s' must include '%s'.", this.value, val);
          }
        }
      }
    }
  }return this;
};
ValueWrapper.prototype.doesNotInclude = ValueWrapper.prototype.notIncludes = function (vals) {
  vals = (0, _dogmalangmin.list)(vals);{
    for (const val of vals) {
      if (_dogmalangmin.dogma.is(this.value, _dogmalangmin.list) && !_dogmalangmin.dogma.includes(["bool", "num", "text", "nil"], (0, _dogmalangmin.typename)(val))) {
        let exists = false;for (const item of this.value) {
          [exists] = _dogmalangmin.dogma.getArrayToUnpack(_dogmalangmin.dogma.peval(() => {
            return assert.deepEqual(item, val);
          }), 1);if (exists) {
            _dogmalangmin.dogma.raise("'%s' must not include '%s'.", this.value, val);
          }
        }
      } else {
        {
          const [ok, res] = _dogmalangmin.dogma.peval(() => {
            return this.value.includes(val);
          });if (ok && res) {
            _dogmalangmin.dogma.raise("'%s' must not include '%s'.", this.value, val);
          }
        }
      }
    }
  }return this;
};
ValueWrapper.prototype.has = function (mems) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("mems", mems, [_dogmalangmin.text, _dogmalangmin.list, _dogmalangmin.map]);{
    if (_dogmalangmin.dogma.isNot(mems, [_dogmalangmin.list, _dogmalangmin.map])) {
      mems = [mems];
    }if (_dogmalangmin.dogma.is(mems, _dogmalangmin.list)) {
      for (const mem of mems) {
        if (_dogmalangmin.dogma.getItem(this.value, mem) == null) {
          _dogmalangmin.dogma.raise("'%s' must have '%s'.", (0, _dogmalangmin.fmt)(this.value), mem);
        }
      }
    } else {
      for (const [mem, val] of Object.entries(mems)) {
        {
          if (!_dogmalangmin.dogma.getItem(_dogmalangmin.dogma.peval(() => {
            return assert.deepEqual(_dogmalangmin.dogma.getItem(this.value, mem), val);
          }), 0)) {
            _dogmalangmin.dogma.raise("%s must have '%s' to %s. Received: %s.", (0, _dogmalangmin.fmt)(this.value), mem, (0, _dogmalangmin.fmt)(val), _dogmalangmin.dogma.getItem(this.value, mem));
          }
        }
      }
    }
  }return this;
};
ValueWrapper.prototype.forEach = function (check) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("check", check, [_dogmalangmin.map, _dogmalangmin.func]);{
    if (_dogmalangmin.dogma.is(check, _dogmalangmin.func)) {
      for (const item of this.value) {
        check(item);
      }
    } else {
      for (const item of this.value) {
        ValueWrapper(item).has(check);
      }
    }
  }return this;
};
ValueWrapper.prototype.doesNotHave = ValueWrapper.prototype.notHas = function (mems) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("mems", mems, [_dogmalangmin.text, _dogmalangmin.list, _dogmalangmin.map]);{
    if (_dogmalangmin.dogma.isNot(mems, [_dogmalangmin.list, _dogmalangmin.map])) {
      mems = [mems];
    }if (_dogmalangmin.dogma.is(mems, _dogmalangmin.list)) {
      for (const mem of mems) {
        if (_dogmalangmin.dogma.getItem(this.value, mem) !== undefined) {
          _dogmalangmin.dogma.raise("%s must not have '%s'.", (0, _dogmalangmin.fmt)(this.value), mem);
        }
      }
    } else {
      for (const [mem, val] of Object.entries(mems)) {
        {
          if (_dogmalangmin.dogma.getItem(this.value, mem) == val) {
            _dogmalangmin.dogma.raise("%s must not have '%s' to '%s'. Received: '%s'.", (0, _dogmalangmin.fmt)(this.value), mem, val, _dogmalangmin.dogma.getItem(this.value, mem));
          }
        }
      }
    }
  }return this;
};
function similar(arr1, arr2) {
  let res = false; /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("arr1", arr1, null); /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("arr2", arr2, null);{
    if ((0, _dogmalangmin.len)(arr1) != (0, _dogmalangmin.len)(arr2)) {
      res = false;
    } else {
      if ((0, _dogmalangmin.len)(arr1) == 0) {
        res = true;
      } else {
        for (const i of arr1) {
          res = false;for (const j of arr2) {
            [res] = _dogmalangmin.dogma.getArrayToUnpack(_dogmalangmin.dogma.peval(() => {
              return assert.deepEqual(i, j);
            }), 1);if (res) {
              break;
            }
          }if (!res) {
            break;
          }
        }
      }
    }
  }return res;
}
ValueWrapper.prototype.similarTo = function (arr) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("arr", arr, null);{
    if (!similar(this.value, arr)) {
      _dogmalangmin.dogma.raise("%s must be similar to %s.", (0, _dogmalangmin.fmt)(this.value), (0, _dogmalangmin.fmt)(arr));
    }
  }return this;
};
ValueWrapper.prototype.notSimilarTo = function (arr) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("arr", arr, null);{
    if (similar(this.value, arr)) {
      _dogmalangmin.dogma.raise("%s must not be similar to %s.", (0, _dogmalangmin.fmt)(this.value), (0, _dogmalangmin.fmt)(arr));
    }
  }return this;
};
ValueWrapper.prototype.isEmpty = function () {
  {
    if (_dogmalangmin.dogma.includes(["bool", "num"], (0, _dogmalangmin.typename)(this.value)) || (0, _dogmalangmin.len)(this.value) > 0) {
      _dogmalangmin.dogma.raise("value must be empty.");
    }
  }return this;
};
ValueWrapper.prototype.isNotEmpty = function () {
  {
    if (_dogmalangmin.dogma.includes(["bool", "num"], (0, _dogmalangmin.typename)(this.value)) || (0, _dogmalangmin.len)(this.value) == 0) {
      _dogmalangmin.dogma.raise("value must not be empty.");
    }
  }return this;
};
ValueWrapper.prototype.len = function (size) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("size", size, [_dogmalangmin.num, _dogmalangmin.list]);{
    let l;if (_dogmalangmin.dogma.is(size, _dogmalangmin.list)) {
      size = (0, _dogmalangmin.len)(size);
    }if (_dogmalangmin.dogma.includes(["bool", "num"], (0, _dogmalangmin.typename)(this.value)) || (l = (0, _dogmalangmin.len)(this.value)) != size) {
      _dogmalangmin.dogma.raise("%s length must be '%s'. Received: %s.", (0, _dogmalangmin.fmt)(this.value), size, l);
    }
  }return this;
};
ValueWrapper.prototype.notLen = function (size) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("size", size, _dogmalangmin.num);{
    if (_dogmalangmin.dogma.includes(["bool", "num"], (0, _dogmalangmin.typename)(this.value)) || (0, _dogmalangmin.len)(this.value) == size) {
      _dogmalangmin.dogma.raise("%s length must not be '%s'.", (0, _dogmalangmin.fmt)(this.value), size);
    }
  }return this;
};
ValueWrapper.prototype.like = function (pat) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("pat", pat, _dogmalangmin.text);{
    if (!RegExp(pat).test(this.value)) {
      _dogmalangmin.dogma.raise("'%s' must be like '%s'.", this.value, pat);
    }
  }return this;
};
ValueWrapper.prototype.notLike = function (pat) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("pat", pat, _dogmalangmin.text);{
    if (RegExp(pat).test(this.value)) {
      _dogmalangmin.dogma.raise("'%s' must not be like '%s'.", this.value, pat);
    }
  }return this;
};
ValueWrapper.prototype.startsWith = function (prefix) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("prefix", prefix, _dogmalangmin.text);{
    if (!(0, _dogmalangmin.text)(this.value).startsWith(prefix)) {
      _dogmalangmin.dogma.raise("'%s' must start with '%s'.", this.value, prefix);
    }
  }return this;
};
ValueWrapper.prototype.doesNotStartWith = ValueWrapper.prototype.notStartsWith = function (prefix) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("prefix", prefix, _dogmalangmin.text);{
    if ((0, _dogmalangmin.text)(this.value).startsWith(prefix)) {
      _dogmalangmin.dogma.raise("'%s' must not start with '%s'.", this.value, prefix);
    }
  }return this;
};
ValueWrapper.prototype.endsWith = function (suffix) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("suffix", suffix, _dogmalangmin.text);{
    if (!(0, _dogmalangmin.text)(this.value).endsWith(suffix)) {
      _dogmalangmin.dogma.raise("'%s' must end with '%s'.", this.value, suffix);
    }
  }return this;
};
ValueWrapper.prototype.doesNotEndWith = ValueWrapper.prototype.notEndsWith = function (suffix) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("suffix", suffix, _dogmalangmin.text);{
    if ((0, _dogmalangmin.text)(this.value).endsWith(suffix)) {
      _dogmalangmin.dogma.raise("'%s' must not end with '%s'.", this.value, suffix);
    }
  }return this;
};