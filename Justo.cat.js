//imports
const {catalog} = require("justo");
const cli = require("justo.plugin.cli");
const fs = require("justo.plugin.fs");
const npm = require("justo.plugin.npm");
const eslint = require("justo.plugin.eslint");
const babel = require("justo.plugin.babel");

//internal data
const pkg = require("./package.json").name;
const who = "justojs";

//catalog
catalog.macro("lint", [
  [cli, "dogmac lint src test"],
  [eslint, "."]
]).title("Lint source code");

catalog.macro("trans-dogma", [
  [cli, "dogmac js --min -o build/src src"],
  [cli, "dogmac js --min -o build/test/unit test/unit"]
]).title("Transpile from Dogma to JS").hidden(true);

catalog.call("trans-js", babel, {
  src: "build",
  dst: `dist/${pkg}/`
}).title("Transpile from JS to JS").hidden(true);

catalog.macro("build", [
  [fs.rm, "./build"],
  [fs.rm, "./dist"],
  catalog.get("trans-dogma"),
  catalog.get("trans-js"),
  [fs.copy, "package.json", `dist/${pkg}/package.json`],
  [fs.copy, "README.md", `dist/${pkg}/README.md`],
]).title("Build package");

catalog.macro("make", [
  catalog.get("lint"),
  catalog.get("build")
]).title("Lint and build");

catalog.call("pub", npm.publish, {
  who,
  access: "public",
  path: `dist/${pkg}`
}).title("Publish on NPM");

catalog.call("install", npm.install, {
  pkg: `./dist/${pkg}`,
  global: true
}).title("Install package globally");

catalog.macro("test", `./dist/${pkg}/test/unit`).title("Unit testing");

catalog.macro("dflt", [
  catalog.get("lint"),
  catalog.get("build"),
  catalog.get("test")
]).title("Lint, build and test");
